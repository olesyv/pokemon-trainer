# PokemonTrainer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.3.

## Heroku link
https://test-cool-app-name92.herokuapp.com/

## NB! Problems

We had some problems showing the image and name for the pokemons catched by a trainer. It shows the correct number, but not the content. We sadly couldnt find a solution for this even after asking others.

We also tried to implement the delete function for catched pokemons, but this made even more problems so we removed it.


## Login Page

Here you can login using your desired name as a Trainer. If u hav already logged in it will recognize you and find your profile. If you are new you will be registered with the entered name and can from now on use the same name to login. After logging in you will be redirected to the pokemon page.


## Pokemon Page

Here you can see a list of available pokemons that you can choose to catch by pressing the circle button under the desired pokemon


## Trainer Page

Here you can se the pokemons you decided to catch. As mentioned under "NB! Problems" this page doesnt show the content for the listed pokemons, but shows a correct number of listed pokemons.


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
